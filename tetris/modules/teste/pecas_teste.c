#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"

#include "../pecas.h"
#include "../pecas.c"

/*Função que adiciona ao registro do CUnit os testes
  paras as funções presentes no arquivo testes.c */

void adicionar_suite(void);

void teste_DT_VerificaDeslocamentoEixoX(void);
void teste_DT_VerificaDeslocamentoEixoY(void);
void teste_DT_VerificaDobroVelocidadeEixoY(void);
void teste_DT_VerificaPecaRotacionada(void);
void teste_DT_VerificaNovaPeca(void);

void teste_DT_VerificaDeslocamentoEixoX(void){
	/*------ Cria nova peca ---*/
	Peca *peca = calloc(1, sizeof(Peca) );

  srand( time(NULL) );
  peca->flag_direcao = rand()%2;
  peca->pos_x = rand()%LIMIT_X;
	/*-------------------------*/

	/*Testa para valores dentro dos limites*/
	move_peca_x(peca, 14);
	CU_ASSERT_EQUAL(peca->pos_x, 14);

	/*Testa para valores excederia o limite direto*/
	move_peca_x(peca, 316);
	CU_ASSERT_EQUAL(peca->pos_x, (LIMIT_X - 1));

	/*Testa para valores excederia o limite esquerdo*/
	move_peca_x(peca, -1000);
	CU_ASSERT_EQUAL(peca->pos_x, 1);

	free(peca);
}


void teste_DT_VerificaDeslocamentoEixoY(void){
	/*------ Cria nova peca ---*/
	Peca *peca = calloc(1, sizeof(Peca) );

  peca->pos_y = 0;
	/*-------------------------*/
	/*Testa para valores dentro dos limites*/
	move_peca_y(peca, 10);
	CU_ASSERT_EQUAL(peca->pos_y, 10);

	/*Testa para valores excederia o limite superior da matrix*/
	move_peca_y(peca, 512);
	CU_ASSERT_EQUAL(peca->pos_y, (LIMIT_Y - 1));

	/*Testa para valores excederia o limite inferior da matrix*/
	move_peca_y(peca, -213);
	CU_ASSERT_EQUAL(peca->pos_y, 1);

	free(peca);
}

void teste_DT_VerificaDobroVelocidadeEixoY(void){
	/*------ Cria nova peca ---*/
	Peca *peca = calloc(1, sizeof(Peca) );
  char caracter_corpo = 97 + rand()%25;
  short int tamanho = 3 + rand()%3;
	int velocidade;

  peca->pos_y = 5;
	/*-------------------------*/
	/*Testa para valores negativos*/
	speed_up(peca, -10);
	CU_ASSERT_EQUAL(peca->pos_y, peca->pos_y);

	/*Testa para valor igual a zero*/
	speed_up(peca, 0);
	CU_ASSERT_EQUAL(peca->pos_y, peca->pos_y);

	/*Testa para valores positivo*/
	velocidade = peca->velocidade;
	speed_up(peca, 2);
	CU_ASSERT_EQUAL(peca->velocidade, velocidade*2);

	free(peca);
}

void teste_DT_VerificaNovaPeca(void){
	/*Corpo teste*/
	Tela *t;
	nova_peca(t);
	/*Testa se nova peça é criada**/
	CU_ASSERT_PTR_NOT_NULL(t->peca);
}

void teste_DT_VerificaPecaRotacionada(void){
	/*Corpo teste*/
	Tela t;
	/*t.peca->corpo[0][0] = [['0', '0', '0', '0'],
									 			['\0','\0','\0','\0'],
									 			['\0','\0','\0','\0'],
									 			['\0','\0','\0','\0']];
	*/
	t.peca->corpo[0][0] = '0';
	t.peca->corpo[0][1] = '0';
	t.peca->corpo[0][2] = '0';
	t.peca->corpo[0][3] = '0';

	t.peca->corpo[1][0] = '\0';
	t.peca->corpo[1][1] = '\0';
	t.peca->corpo[1][2] = '\0';
	t.peca->corpo[1][3] = '\0';

	t.peca->corpo[2][0] = '\0';
	t.peca->corpo[2][1] = '\0';
	t.peca->corpo[2][2] = '\0';
	t.peca->corpo[2][3] = '\0';

	t.peca->corpo[3][0] = '\0';
	t.peca->corpo[3][1] = '\0';
	t.peca->corpo[3][2] = '\0';
	t.peca->corpo[3][3] = '\0';
	/*Testa rotação*/
	rotaciona_peca(t.peca);
	CU_ASSERT_EQUAL(t.peca->corpo[0][0], '\0');
	CU_ASSERT_EQUAL(t.peca->corpo[0][1], '\0');
	CU_ASSERT_EQUAL(t.peca->corpo[0][2], '\0');
	CU_ASSERT_EQUAL(t.peca->corpo[0][3], '\0');

	rotaciona_peca(t.peca);
	CU_ASSERT_EQUAL(t.peca->corpo[3][0], '0');
	CU_ASSERT_EQUAL(t.peca->corpo[3][1], '0');
	CU_ASSERT_EQUAL(t.peca->corpo[3][2], '0');
	CU_ASSERT_EQUAL(t.peca->corpo[3][3], '0');
}

void adicionar_suite(void){
	CU_pSuite suite;

	/*Cria uma suite que conterá todos os testes*/
	suite = CU_add_suite("Testes da mo_pecas",NULL,NULL);

	CU_ADD_TEST(suite, teste_DT_VerificaDeslocamentoEixoX);
	CU_ADD_TEST(suite, teste_DT_VerificaDeslocamentoEixoY);
	CU_ADD_TEST(suite, teste_DT_VerificaDobroVelocidadeEixoY);
	// CU_ADD_TEST(suite, teste_DT_VerificaPecaRotacionada);
	CU_ADD_TEST(suite, teste_DT_VerificaNovaPeca);

}

int main(void){
	/*Inicializa o registro de suítes e testes do CUnit*/
	if (CUE_SUCCESS != CU_initialize_registry())
    		return CU_get_error();

  /*Adiciona os testes ao registro*/
  adicionar_suite();

	/*Muda o modo do CUnit para o modo VERBOSE
	 O modo VERBOSE mostra algumas informacoes a
	 mais na hora da execucao. Outras opções: NORMAL e SILENT*/
	CU_basic_set_mode(CU_BRM_VERBOSE);

	/*Roda os testes e mostra na tela os resultados*/
	CU_basic_run_tests();
	//CU_console_run_tests();
	//CU_automated_run_tests();


	/*Limpa o registro*/
	CU_cleanup_registry();

	return CU_get_error();
}
