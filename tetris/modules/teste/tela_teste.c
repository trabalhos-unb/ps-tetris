#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"

#include "../tela.h"
#include "../tela.c"
#include "../pecas.h"
#include "../pecas.c"

void adicionar_suite(void);

void teste_cria_tela(void);
void teste_verifica_peca_limitex(void);
void teste_verifica_peca_limitey(void);
void teste_verifica_peca_limitex_2(void);
void teste_verifica_peca_limitey_2(void);
void teste_retirar_linha(void);

void adicionar_suite(void) {
	CU_pSuite suite;

	suite = CU_add_suite("Testes da smo_datas",NULL,NULL);


	CU_ADD_TEST(suite, teste_cria_tela);
	CU_ADD_TEST(suite, teste_verifica_peca_limitex);
	CU_ADD_TEST(suite, teste_verifica_peca_limitey);
	CU_ADD_TEST(suite, teste_verifica_peca_limitex_2);
	CU_ADD_TEST(suite, teste_verifica_peca_limitey_2);
	CU_ADD_TEST(suite, teste_retirar_linha);
}

/*! Verifica a criação do frame */
void teste_cria_tela(){
	Tela* resultado;
	resultado = cria_tela();
	/*! verifica se o tela esta vazio */
	CU_ASSERT(resultado != NULL);
}

/*! Verifica o primeiro caso do limite x */
void teste_verifica_peca_limitex(){
	short int resultado;
	Tela* tela;
	tela = cria_tela();
	nova_peca(tela);
	verifica_peca_limitex(tela);
	resultado = tela->peca->pos_x;
	/*! verifica se está dentro desse limite */
	CU_ASSERT(resultado < 16 && resultado >= 1);
}

/*! Verifica o primeiro caso do limite y */
void teste_verifica_peca_limitey(){
	short int resultado;
	Tela* tela;
	tela = cria_tela();
	nova_peca(tela);
	verifica_peca_limitey(tela);
	resultado = tela->peca->pos_y;
	/*! verifica se está dentro desse limite */
	CU_ASSERT(resultado < 26 && resultado >= 1);
}
/*! Verifica o segundo caso do limite x */
void teste_verifica_peca_limitex_2(){
	short int resultado;
	Tela* tela;
	tela = cria_tela();
	nova_peca(tela);
	tela->peca->pos_x = 16;
	verifica_peca_limitex(tela);
	resultado = tela->peca->pos_x;
	/*! verifica se está dentro desse limite */
	CU_ASSERT(resultado == 16 || resultado < 1);
}

/*! Verifica o segundo caso do limite y */
void teste_verifica_peca_limitey_2(){
	short int resultado;
	Tela* tela;
	tela = cria_tela();
	nova_peca(tela);
	tela->peca->pos_y = 26;
	verifica_peca_limitey(tela);
	resultado = tela->peca->pos_y;
	CU_ASSERT(resultado == 26 || resultado < 1);
}

/*! Verifica a função de retirar a linha */
void teste_retirar_linha(){
	int resultado;
	Tela* tela;
	tela = cria_tela();
	resultado = retirar_linha(tela, 4);
	CU_ASSERT(resultado == -1);
}

/*! Testes de funções que são apenas printf não precisam ser testadas, como o de mostrar a tela, inicio, tutorial e o fim. O critério que tentamos utilizar no módulo tela foi por cobertura de decisões, onde cada forma de avaliar expressões lógicas é exercitada pelo menos uma vez no conjunto de todos os casos de teste */
int main(void){
	if (CUE_SUCCESS != CU_initialize_registry()) {
    return CU_get_error();
	}

  	adicionar_suite();

	CU_basic_set_mode(CU_BRM_VERBOSE);

	CU_basic_run_tests();

	CU_cleanup_registry();

	return CU_get_error();
}
