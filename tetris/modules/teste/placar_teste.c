#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"

#include "../placar.h"
#include "../placar.c"


void adicionar_suite(void);

void teste_verifica_arquivo_vazio(void);
void teste_verifica_arquivo_10_linhas(void);
void teste_mostra_placar_MAIOR(void);
void teste_mostra_placar_MENOR(void);
void teste_mostra_placar_MEIO(void);
void teste_limite_5_arquivo(void);

void limpa_arquivo(void);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*!
*	Teste pra ver se o arquivo é nulo ou não.
*	O critério utilizado para esse teste foi o de instruções, garantindo que o arquivo placar.txt nunca será nulo.
*/
void teste_verifica_arquivo_nulo(void){
	int resultado;
	cria_placar();
	FILE* fp = fopen("placar.txt", "r");
	resultado = verifica_arquivo(fp);
	CU_ASSERT_EQUAL(resultado, 0);
	fclose(fp);
}

/*!
*	Teste verificando que a contagem de linhas do arquivo placar.txt está correta.
*	O critério utilizado para esse teste foi o de instruções, garantindo que para um valor menor que o encontrado no arquivo, ele irá ser posicionado corretamente.
*/
void teste_verifica_arquivo_10_linhas(void){
	int resultado;
	FILE* fp = fopen("arquivosTxt/placar10.txt", "r");
	resultado = verifica_arquivo(fp);
	CU_ASSERT_EQUAL(resultado, 10);
	fclose(fp);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*!
*	Teste de inserção de um placar no arquivo placa.txt.
*	O critério utilizado para esse teste foi o de instruções, garantindo que para um valor maior que o encontrado no arquivo, ele irá ser posicionado corretamente.
*/
void teste_mostra_placar_MAIOR(void) {
	Ranking* rank;
	char nome[60] = "Teste";
	cria_placar();
	atualiza_placar(110, nome, 10);
	rank = mostra_placar();
	CU_ASSERT_EQUAL(rank[0].pontuacao, 110);
}

/*!
*	Teste de inserção de um placar menor que o placar que já se encontra no arquivo placa.txt.
*	O critério utilizado para esse teste foi o de instruções, garantindo que para um valor menor que o encontrado no arquivo, ele irá ser posicionado corretamente.
*/
void teste_mostra_placar_MENOR(void) {
	Ranking* rank;
	char nome[60] = "Teste";
	cria_placar();
	atualiza_placar(90, nome, 10);
	rank = mostra_placar();
	CU_ASSERT_EQUAL(rank[1].pontuacao, 90);
}

/*!
*	Insere um placar entre o maior e o menor placar no placar.txt.
*	O critério utilizado para esse teste foi o de instruções, garantindo que para um valor que não fosse nem o maior nem o menor encontrado no arquivo, ele irá ser posicionado corretamente.
*/
void teste_mostra_placar_MEIO(void) {
	Ranking* rank;
	char nome[60] = "Teste";
	cria_placar();
	atualiza_placar(100, nome, 10);
	rank = mostra_placar();
	CU_ASSERT_EQUAL(rank[1].pontuacao, 100);
}

/*!
*	Teste verificando que o limite máximo de linhas no arquivo placar.txt é de 5 linhas.
* O critério utilizado para esse teste foi o de instruções, garantindo que o máximo de placares salvos será de 5 placares.
*/
void teste_limite_5_arquivo(void) {
	int linhas = 0;
	cria_placar();
	atualiza_placar(100, "nome", 10);
	cria_placar();
	atualiza_placar(1000, "nome", 10);
	cria_placar();
	atualiza_placar(250, "nome", 10);
	cria_placar();
	atualiza_placar(1002, "nome", 10);
	FILE* fp = fopen("placar.txt", "r");
	linhas = verifica_arquivo(fp);
	fclose(fp);
	CU_ASSERT_EQUAL(linhas, 5);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void adicionar_suite(void) {
	CU_pSuite suite;

	suite = CU_add_suite("Testes da smo_datas",NULL,NULL);

	CU_ADD_TEST(suite, teste_verifica_arquivo_nulo);
	CU_ADD_TEST(suite, teste_verifica_arquivo_10_linhas);
	CU_ADD_TEST(suite, teste_mostra_placar_MAIOR);
	CU_ADD_TEST(suite, teste_mostra_placar_MENOR);
	CU_ADD_TEST(suite, teste_mostra_placar_MEIO);
	CU_ADD_TEST(suite, teste_limite_5_arquivo);

}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*!
* O gcov não possui cobertura completa. As linhas que ele não cobriu são as de verificação caso o arquivo seja nulo, contudo como o arquivo nunca é nulo, devido a sua implementação, é esperado que essas linhas nunca sejam percorridas
*/
int main(void){
	if (CUE_SUCCESS != CU_initialize_registry()) {
    return CU_get_error();
	}

	limpa_arquivo();

  adicionar_suite();

	CU_basic_set_mode(CU_BRM_VERBOSE);

	CU_basic_run_tests();

	CU_cleanup_registry();

	return CU_get_error();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void limpa_arquivo(void) {
	fclose(fopen("placar.txt", "w"));
}
