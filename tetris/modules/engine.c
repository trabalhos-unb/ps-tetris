#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include "engine.h"


void inicia_engine(){
  if (initscr() == NULL) {
    printf("ERROR na inicia_engine()\n");
    exit(0);
  }
  raw();
  keypad(stdscr, TRUE); // Permite pegar as setas como comando
  noecho(); // Não printa os valores do getch
}

void termina_engine(){
  endwin();
}

// Funcao para pegar o input do teclado
int pega_input(){

    switch(getch()){
  			case KEY_LEFT:
          // printw("Tecla esquerda\n");
          return 0;
        break;

        case KEY_RIGHT:
          // printw("Tecla direita\n");
          return 1;
        break;

        case KEY_DOWN:
          // printw("Tecla baixo\n");
          return 2;
        break;

        case KEY_UP:
          // printw("Tecla cima\n");
          return 3;
        break;

        case 'q':
          return 4;
        break;

        default:
          // printw("Entrada invalida\n");
        break;
		}
}


// int main() {
//
//   inicia_engine();
//
//   pega_input();
//
//   getch();
//
//   termina_engine();
//
// }
