#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "placar.h"

static FILE* file_placar;

static Ranking ranking[6];
static Ranking ranking_novo[6];
static Ranking jogador_atual;

static int linhas_arquivo = 0;

static char* nome_arquivo = "placar.txt";

// Assinatura das funções
void cria_placar();
void atualiza_placar(int pontuacao, char* player, double tempo_jogado);
Ranking* mostra_placar();

static int verifica_arquivo(FILE* fp);
static void leitura_placar();
static void atualiza_array_ranking(Ranking jogador_atual);
static void printa_placar_file();
static void organiza_ordem_crescente();
////////////////////////////////////////////////////////////////////////////////////////////////////
static int verifica_arquivo(FILE* fp) {
  int i = 0;
  char ch;

  // While que conta a quantidade de linhas no arquivo de placar
  while (!feof(fp)) { // while EOF
    ch = fgetc(fp);
    if(ch == '\n') { // if ch == '\n'
      i++;
    } // if ch == '\n'
  } // while EOF

  return i;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
void cria_placar() {
  file_placar = fopen(nome_arquivo, "a+");

  linhas_arquivo = verifica_arquivo(file_placar);

  fclose(file_placar);
}
////////////////////////////////////////////////////////////////////////////////////////////////////
void atualiza_placar(int pontuacao, char* player, double tempo_jogado) {

  strcpy(jogador_atual.player, player);
  jogador_atual.tempo_jogado = tempo_jogado;
  jogador_atual.pontuacao    = pontuacao;

  leitura_placar();

  file_placar = fopen(nome_arquivo, "r+");

  if (file_placar == NULL) {
    printf("Deu ruim 2\n");
    exit(-1);
  }

  if (ferror(file_placar)){
    printf("Arquivo	placar.txt vazio!\n");
    exit(-1);
  }

  atualiza_array_ranking(jogador_atual);

  organiza_ordem_crescente();

  fclose(file_placar);

  // Apaga o arquivo todo para reescrever
  file_placar = fopen(nome_arquivo, "w");

  printa_placar_file();

  fclose(file_placar);

}
////////////////////////////////////////////////////////////////////////////////////////////////////
static void leitura_placar() {

  char a[60];
  double c = 0;
  int b = 0;
  int i = 0;

  FILE* fp = fopen(nome_arquivo, "r");

  rewind(fp);

  if (fp == NULL) {
    printf("Deu ruim\n");
    exit (-1);
  }

  if (linhas_arquivo > 0) {
    for (; i < linhas_arquivo; i++) {
      fscanf(fp, "%s\t%d\t%lf\n", a, &b, &c);
      strcpy(ranking[i].player, a);
      ranking[i].tempo_jogado = c;
      ranking[i].pontuacao = b;
    } // for
  } // if


  fclose(fp);

}
////////////////////////////////////////////////////////////////////////////////////////////////////
static void atualiza_array_ranking(Ranking jogador_atual) {

  ranking[linhas_arquivo].pontuacao    = jogador_atual.pontuacao;
  strcpy(ranking[linhas_arquivo].player, jogador_atual.player);
  ranking[linhas_arquivo].tempo_jogado = jogador_atual.tempo_jogado;

}
////////////////////////////////////////////////////////////////////////////////////////////////////
static void printa_placar_file() {

  int i = 0;

  for (; i < linhas_arquivo+1; i++) {
    if (i == 5) break;
    fprintf(file_placar, "%s\t%d\t%lf\n", ranking_novo[i].player, ranking_novo[i].pontuacao, ranking_novo[i].tempo_jogado);
  } // for i

}
////////////////////////////////////////////////////////////////////////////////////////////////////
static void organiza_ordem_crescente() {

  int aux = -1;
  int pos = -1;

  int j = 0;

  for (; j <= linhas_arquivo; j++) { // for j
    int i = 0;
    for (; i <= linhas_arquivo; i++) { // for i
      if (ranking[i].pontuacao >= aux) { // if
        aux = ranking[i].pontuacao;
        pos = i;
      } // if
    } // for i

    ranking_novo[j].pontuacao    = ranking[pos].pontuacao;
    ranking_novo[j].tempo_jogado = ranking[pos].tempo_jogado;
    strcpy(ranking_novo[j].player, ranking[pos].player);

    ranking[pos].pontuacao = 0;
    aux = -1;
    pos = -1;
  } // for j

}
////////////////////////////////////////////////////////////////////////////////////////////////////
Ranking* mostra_placar() {
  return ranking_novo;
}
////////////////////////////////////////////////////////////////////////////////////////////////////
