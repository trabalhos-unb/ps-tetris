#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "pecas.h"
#include "tela.h"

void corpo_peca(char **corpo){
  srand( time(NULL) );
  int x, y, escolha = 0;//rand()%7;

  char pecas[][4][4] = {
                        {{'0', '0', '0', '0'},
                        {'\0','\0','\0','\0'},
                        {'\0','\0','\0','\0'},
                        {'\0','\0','\0','\0'}}
                        ,
                        {{'\0', '0', '0','\0'},
                        { '\0', '0', '0','\0'},
                        { '\0','\0','\0','\0'},
                        { '\0','\0','\0','\0'}}
                        ,
                        {{'\0', '\0', '0','0'},
                        { '\0', '0', '0','\0'},
                        { '\0','\0','\0','\0'},
                        { '\0','\0','\0','\0'}}
                        ,
                        {{'\0', '0', '0','0' },
                        { '\0','\0', '0','\0'},
                        { '\0','\0','\0','\0'},
                        { '\0','\0','\0','\0'}}
                      };


  for(x = 0 ; x < 4 ; x++ )
    for(y = 0 ; y < 4 ; y++ ){
      corpo[x][y] = pecas[escolha][x][y];
    }

}

void nova_peca(Tela* tela){
  Peca *nova_peca = calloc(1, sizeof(Peca) );
  short int temp;

  nova_peca->corpo = calloc(4, sizeof(char *));
  for(temp = 0 ; temp < 4 ; temp++)
    nova_peca->corpo[temp] = calloc(4, sizeof(char) );

  corpo_peca(nova_peca->corpo);

  srand( time(NULL) );

  nova_peca->tamanho = 4;
  nova_peca->flag_direcao = rand()%2;
  nova_peca->pos_y = 2;
  nova_peca->pos_x = 4;
  nova_peca->velocidade = 1;

  tela->peca = nova_peca;
}

void move_peca_x(Peca* peca, int x){
  /*Evitar passagem de valores erroneos*/
  if( x > LIMIT_X)
    x = LIMIT_X - 1;
  else if( x < 1 )
    x = 1;

  peca->pos_x = x;
}

void move_peca_y(Peca* peca, int y){
  /*Evitar passagem de valores erroneos*/
  if( y > LIMIT_Y)
    y = LIMIT_Y - 1;
  else if( y < 1 )
      y = 1;

  peca->pos_y = y;
}

void rotaciona_peca(Peca* peca){
  int x, y, temp;
  char **corpo_rot;

  corpo_rot = calloc(4, sizeof(char *) );
  for(temp = 0 ; temp < 4 ; temp++){
    corpo_rot[temp] = calloc(4, sizeof(char) );
  }

  for(x = 0 ; x < 4 ; x++ ){
    for(y = 0 ; y < 4 ; y++ ){
      corpo_rot[x][y] = peca->corpo[3-y][x];
    }
  }

  /*Passa corpo rotacionado*/
  peca->corpo = corpo_rot;

}

void speed_up (Peca* peca, int y){
  if( y <= 0){
    //Do nothing
  }else{
    peca->velocidade = peca->velocidade*y;
  }
}
