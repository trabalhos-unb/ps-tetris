#ifndef PECAS_MOD
#define PECAS_MOD

#ifndef STRUCT_PECA
#define STRUCT_PECA
  #define HORIZONTAL 0
  #define VERTICAL 1
  typedef struct peca{
    char **corpo;
    short int pos_x;
    short int pos_y;
    char flag_direcao;
    short int tamanho;
    short int velocidade;
  }Peca;
#endif //STRUCT_PECA

#ifndef STRUCT_TELA
#define STRUCT_TELA
  #define LIMIT_X 16
  #define LIMIT_Y 26
  typedef struct tela{
    char FRAME[LIMIT_X][LIMIT_Y];
    Peca* peca;
  }Tela;
#endif //STRUCT_TELA

void nova_peca(Tela* tela);

void move_peca_x(Peca* peca, int x);

void move_peca_y(Peca* peca, int y);

void rotaciona_peca(Peca* peca);

void speed_up (Peca* peca, int y);

#endif //PECAS_MOD
