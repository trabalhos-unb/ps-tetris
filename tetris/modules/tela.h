#ifndef TELA_MOD
#define TELA_MOD

#define LIMIT_X 16
#define LIMIT_Y 26
#define LIMIT_SUP 5

#ifndef STRUCT_PECA
#define STRUCT_PECA
  #define HORIZONTAL 0
  #define VERTICAL 1
  typedef struct peca{
    char **corpo;
    short int pos_x;
    short int pos_y;
    char flag_direcao;
    short int tamanho;
    short int velocidade;
  }Peca;
#endif //STRUCT_PECA

#ifndef STRUCT_TELA
#define STRUCT_TELA
  typedef struct tela{
    char FRAME[LIMIT_X][LIMIT_Y];
    Peca* peca;
  }Tela;
#endif //STRUCT_TELA

void jogo_inicio();
void tutorial();
void jogo_fim();
Tela* cria_tela();
void mostra_tela(Tela* t);
int verifica_peca_limitex(Tela* t);
int verifica_peca_limitey(Tela*t);
void linha_completa(Tela* t);
int retirar_linha(Tela* t, int y);
#endif
