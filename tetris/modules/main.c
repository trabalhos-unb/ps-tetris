#include "main.h"

int pont = 0;

int main(int argc, char const *argv[]) {

  Tela* tela;
  int loop = 1, temp;
  int status_jogo = 1;

  char jogador[60];

  double tempo = 0;

  // time_h tempo_inicio;
  // time_h tempo_fim;

  printf("Digite o nome do jogador:\n");
  scanf("%s\n", jogador);

  // Função para inicializar a engine do jogo
  inicia_engine();

  // Tela de início do jogo
  jogo_inicio();
  tutorial();

  //tempo_inicio = time(NULL);

  cria_placar();
  // A função “cria_tela” deve criar a interface(bordas), lugar onde ocorrerá toda a dinâmica do jogo
  tela = cria_tela();
  nova_peca(tela);
  // Carrega a matriz mais atualizada possível dentro do frame do jogo
  mostra_tela(tela);

  // TODO: criar a funcao para verificar qnd o jogo acaba
  while (loop) {

    nova_peca(tela);

    // TODO: criar funcao para verficar qnd criar a nova peca
    while(verifica_peca_limitey(tela)) {

      switch ( pega_input() ) {

        // Move peca para a esquerda
        case 0: move_peca_x(tela->peca, tela->peca->pos_x-1);
        break;

        // Move peca para a direita
        case 1:  move_peca_x(tela->peca, tela->peca->pos_x+1);
        break;

        // Move peca para baixo
        case 2: move_peca_y(tela->peca, tela->peca->pos_y + (tela->peca->velocidade));
        break;

        // Gira a peca
        case 3: rotaciona_peca(tela->peca);
        break;

        case 4:
        loop = 0;
        break;

        default:
        break;

      } // switch do input

      mostra_tela(tela);
      linha_completa(tela);
    } // while de peca

  } // while do jogo
  /*
  tempo_fim = time(NULL);

  tempo = difftime(tempo_fim, tempo_jogado);
  */
  atualiza_placar(pont, jogador, tempo);

  // Função para finalizar o uso da engine do jogo
  jogo_fim();
  termina_engine();

  for(temp = 0 ; temp < 4 ; temp++ ){
    free(tela->peca->corpo[temp]);
  }
  free(tela->peca->corpo);
  free(tela->peca);
  free(tela);

return 0;

}
