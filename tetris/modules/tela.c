#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>

#include "pecas.h"
#include "tela.h"
#include "placar.h"

void jogo_inicio(){

	start_color();
	init_pair(1,COLOR_MAGENTA, COLOR_BLACK);
	attrset(COLOR_PAIR(1));
	printw(" _______   _______    _______    _______     _      _______  \n");
	printw("|_||_||_| |_||_||_|  |_||_||_|  |_||_||_|   |_|    |_||_||_| \n");
	printw("   |_|	  |_| _  _      |_|     |_|   |_|   |_|    |_|_____  \n");
	printw("   |_|    |_||_||_|     |_|     |_||_||_|   |_|    |_||_||_| \n");
	printw("   |_|    |_| _  _      |_|     |_||_| _    |_|     _____|_| \n");
	printw("   |_|    |_||_||_|     |_|     |_|   |_|   |_|    |_||_||_| \n\n");
	printw("                  BEM VINDO AO TETRIS!!!\n");
	printw("              aperte enter para continuar");
	getch();
	clear();
}

void jogo_fim(){

	start_color();
	init_pair(1,COLOR_CYAN, COLOR_BLACK);
	attrset(COLOR_PAIR(1));
	printw("GAME OVER!!!\nAperte alguma tecla para sair...");
	getch();
}
void tutorial(){

	start_color();
	init_pair(1,COLOR_GREEN, COLOR_BLACK);
	attrset(COLOR_PAIR(1));
	printw("Tutorial:\n");
	printw("Seta para cima[^]: Rotaciona\nSeta para esquerda[<]: move para esquerda\nSeta para direita[>]: move para direito\nSeta para baixo[V]: aumenta velocidade de queda\nQ: sai do jogo");
	getch();
	clear();
}

Tela* cria_tela(){
	int i=0, j=0;
	Tela* tela = malloc(sizeof(Tela));

	for (i = 0; i < LIMIT_X; i++){
		for (j = 0; j < LIMIT_Y; j++){
			if(i == 0){
				tela->FRAME[i][j]='-';
			}

			if( (i > 0 && i < 15 ) && j == 0){
				if(i == LIMIT_SUP){
					tela->FRAME[i][j]='>';
				}
				else{
					tela->FRAME[i][j]='|';
				}
			}

			if( (i > 0 && i < 15) && j == 25){
				if(i == LIMIT_SUP){
					tela->FRAME[i][j]='<';
				}
				else{
					tela->FRAME[i][j]='|';
				}
			}

			if(i == 15){
				tela->FRAME[i][j]='-';
			}
			if((i > 0 && i < 15) && (j > 0 && j < 25)){
				tela->FRAME[i][j]=' ';
			}
		}
	}
	return(tela);
}

void mostra_tela(Tela* t){
	init_pair(1,COLOR_BLUE, COLOR_BLACK);
	attrset(COLOR_PAIR(1));

	int pos_x = t->peca->pos_x, pos_y = t->peca->pos_y;
	int i, j, z = 0, l = 0;

	for(i = 0 ; i < LIMIT_X ; i++){
		for(j = 0 ; j < LIMIT_Y ; j++){
			//Imprime pecas
			if( (i == (pos_x + z)) && (j == (pos_y + l)) ){
				printw("%c", t->peca->corpo[z][l]);
			}else{
				// Imprime restante matriz
				printw("%c",t->FRAME[i][j]);
			}
		}
		printw("\n");
	}
	getch();
	clear();
}

int verifica_peca_limitex(Tela* t){
	int x, y;
	if(t->peca->pos_x + t->peca->tamanho >= LIMIT_X){
		for( x = 0 ; t->peca->pos_x + x < LIMIT_X ; x++ ){
			t->FRAME[t->peca->pos_x + x][t->peca->pos_y] = t->peca->corpo[x][0];
		}
		return 1;
	}else{
		return 0;
	}
}
int verifica_peca_limitey(Tela* t){
	int x, y;
	if(t->peca->pos_y + t->peca->tamanho >= LIMIT_Y){
		for( y = 0 ; t->peca->pos_y + y < LIMIT_Y ; y++ ){
			t->FRAME[t->peca->pos_x][t->peca->pos_y + y] = t->peca->corpo[0][y];
		}
		return 1;
	}else{
		return 0;
	}
}
int retirar_linha(Tela* t ,int y){
	int x;
	if(y < 5 || y > LIMIT_Y){
		return (-1);
	}
	for(; y >= 1; y--){
		for(x=0; x < LIMIT_X; x++){
			t->FRAME[x][y] = t->FRAME[x][y-1];// apaga a última linha descendo as anteriores
		}
	}


	return (0);
}

void linha_completa(Tela* t) {
	int x, y, contador;
	for(y = 5; y < LIMIT_Y; y++){
		contador=0;
		for(x = 0; x < LIMIT_X; x++){
			if(t->FRAME[x][y] != 0){ // ele verifica cada linha para ver se está cheia
			contador++;
			}
		}
		if(contador == LIMIT_X){
			retirar_linha(t, y);
			//pont = pont + 100; //incrementa pontuação
			
		}
	}
}
