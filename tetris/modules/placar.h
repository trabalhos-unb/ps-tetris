#ifndef STRUCT_RANKING
#define STRUCT_RANKING

typedef struct {
  char player[60];
  double tempo_jogado;
  int pontuacao;
} Ranking;

#endif // STRUCT_RANKING

void cria_placar();
void atualiza_placar(int pontuacao, char* player, double tempo_jogado);
Ranking* mostra_placar();
