# ps-tetris
Para compilação do trabalho ps-tetris, é necessario ter em seu computador :
    * Biblioteca ncurses
``` bash
      sudo apt-get install ncurses
```
Após completados os requisitos acima, faça uma cópia do repositório :
``` bash
      git clone https://github.com/santosdanilo/ps-tetris
```
Para compilar entre na pasta ps-tetris/tetris, e digitar o comando make :
``` bash
      cd ps-tetris/tetris
      make
```
Para executar, supondo que você esteja na pasta /tetris:
``` bash
    ./tetris.exe
```
##Os comandos do jogo são
	"^" rotaciona a peça,
	">" move a peça pra direita,
	"v" move a peça para baixo,
	"<" move a peça pra esquerda.

##Testes e gcov
  Para rodar os testes e o gcov, basta entrar na pasta /tetris/modules/teste e rodar os seguintes comandos:
  ``` bash
    make placar
    make placar_gcov
    make tela
    make tela_gcov
    make pecas
    make pecas_gcov
  ```

##Doxygen
  Os arquivos do Doxygen se encontram na pastas /tetris/modules/html/index.html, para a documentação do projeto, e para a documentação dos testes, /tetris/modules/testes/html/index.html

##Membros
	Afonso Dias 14/0055771
	Danilo Santos 14/0135910
	Rafael Dias 14/0030433
